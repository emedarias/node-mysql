const { DataTypes } = require('sequelize');

//Se define la estructura del modelo (o sea, la tabla de Bandas en la base de datos) en esta coneccion realizada.
function createBandModel(connection)
	{
		//Band posee un id unico y autoincremental, un nombre, cantida de integrantes, la fecha de inicio y de separacion (que puede existir o no) y el pais.
		const Band = connection.define('Band',{
			id: {
		    type: DataTypes.INTEGER,
		    allowNull: false,
		    primaryKey: true,
		    autoIncrement: true
		  },
		  	nombre: {
		  	type: DataTypes.STRING,
		  	allowNull: false
		  },
		  	integrantes: {
		  	type: DataTypes.INTEGER,
		  	allowNull: false
		  },
		  	fecha_inicio: {
		  	type: DataTypes.DATE,
		  	allowNull: false
		  },
		  	fecha_separacion: {
		  	type: DataTypes.DATE,
		  	allowNull: true
		  },
		  	pais: {
		  	type: DataTypes.STRING,
		  	allowNull: false
		  },	
		});

		return Band;
	}


module.exports = {createBandModel};