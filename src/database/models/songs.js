const {DataTypes} = require('sequelize')

//Se define la estructura del modelo (o sea, la tabla de Canciones en la base de datos) en esta coneccion realizada.
function createSongModel(connection)
	{
		//Song posee un id unico y autoincremental, un nombre, duracion y la fecha en que se publico.
		const Song = connection.define('Song', {
			id : {
				type: DataTypes.INTEGER,
				allowNull:false,
				autoIncrement:true,
				primaryKey:true
			},
			nombre : {
				type: DataTypes.STRING,
				allowNull:false
			},
			duracion : {
				type: DataTypes.INTEGER,
				allowNull:false
			},
			fecha_publicacion : {
				type: DataTypes.DATE,
				allowNull:false
			}
		});

		return Song;
	}

module.exports = {createSongModel};