const {DataTypes} = require('sequelize');

//Se define la estructura del modelo (o sea, tabla de la relacion entre Albums y Canciones en la base de datos) en esta coneccion realizada.
function createAlbumSongModel(connection, Album, Song)
	{
		//AlbumSong posee una posicion del tema en el album, el id del album y el id de la canción.
		const AlbumSong = connection.define('AlbumSong',{
			posicion:{
				type: DataTypes.INTEGER,
				allowNull: true
			},
			AlbumId:{
				type: DataTypes.INTEGER,
				allowNull: false,
				reference:{
					model: Album,
					key: 'id'
				}
			},
			SongId:{
				type: DataTypes.INTEGER,
				allowNull: false,
				reference:{
					model: Song,
					key: 'id'
				}
			}
		},
		{
			modelName: 'AlbumSong',
			tableName:'AlbumSong'	
		}			
		);
		return AlbumSong;
	}

module.exports = {createAlbumSongModel};