const {DataTypes} = require('sequelize');

//Se define la estructura del modelo (o sea, tabla de Albums en la base de datos) en esta coneccion realizada.
function createAlbumModel(connection)
	{
		//Album posee un id unico y autoincremental, un nombre y la fecha en que se publico.
		const Album = connection.define('Album',{
			id:{
				type: DataTypes.INTEGER,
				allowNull: false,
				autoIncrement:true,
				primaryKey:true
			},
			nombre:{
				type: DataTypes.STRING,
				allowNull:false
			},
			fecha_publicacion:{
				type: DataTypes.DATE,
				allowNull:false
			}
		});

		return Album;
	}

module.exports = {createAlbumModel};