const { Sequelize } = require('sequelize');
const {createBandModel} = require('./models/bands.js');
const {createSongModel} = require('./models/songs.js');
const {createAlbumModel} = require('./models/albums.js');
const {createAlbumSongModel} = require('./models/albumSongs.js');

const models = {};

async function connect(host, port, username, password, database)
	{
		//Se realiza la coneccion a la base de datos, para ello se envian todos los datos necesarios. 
		const connection = new Sequelize({
			database,
			username,
			password,
			host,
			port,
			dialect : 'mariadb',
			//para que no se vean los logs al hacer la coneccion con la BD.
			logging:false
		});
		//Se agrega el modelo "Band" al objeto "models".
		models.Band = createBandModel(connection);
		//Se agrega el modelo "Song" al objeto "models".
		models.Song = createSongModel(connection);
		//Se agrega el modelo "Album" al objeto "models".
		models.Album = createAlbumModel(connection);
		//Se agrega el modelo "AlbumSong" al objeto "models", enviando como datos necesarios el modelo "Album" y el "Song".
		models.AlbumSong = createAlbumSongModel(connection, models.Album, models.Song);

		//Crea la relacion de que una banda, tiene muchos albumes.
		models.Band.hasMany(models.Album);
		//Y a su vez, le decimos que un album pertenece a una sola banda.
		models.Album.belongsTo(models.Band);

		//Crea la relacion de que una banda, tiene muchas canciones.
		models.Band.hasMany(models.Song);
		//Y a su vez, le decimos que una cancion pertenece a una sola banda.
		models.Song.belongsTo(models.Band);

		//Crea la relacion de que un album, pertenece a muchas canciones.
		models.Album.belongsToMany(models.Song,{through:models.AlbumSong});
		//Y que una cancion, pertenece a muchos albumes.
		models.Song.belongsToMany(models.Album,{through:models.AlbumSong});

		
		try
			{
				//Se autentica y establece la coneccion con la BD.
				await connection.authenticate();
				//Con sync se sincronizan los modelos definidos y si no exiten, los crea.
				await connection.sync();

				console.log("Connection done successfully! :)");
				return true;
			}
		catch(error)
			{
				console.error("Unable to connect to the database! :(",error);
				return false;
			}
	}

function getModel(nombre)
	{
		if(models[nombre])
			{
				return models[nombre];
			}
		else
			{
				console.error("Model "+nombre+" doesn't exist.")
				return null;
			}
	}

module.exports={connect, getModel};