const {getModel} = require('../database/index.js');

async function getBands(req,res)
	{
		try
			{
				const band = await getModel('Band').findAll();
				if(band.length>0)
					{
						return res.status(200).json(band);
					}
				else
					{
						return res.status(404).send('There are no Bands registered. :(');
					}
						
			}
		catch(error)
			{
				return res.status(400).send(error);
			}
				
	}

async function getBandsNombre(req,res)
	{
		try
			{
				const band = await getModel('Band').findOne({where:{nombre:req.body.nombre}});
				if(band===null)
					{
						return res.status(404).send('There are no Bands registered with this name. :(');
					}
				else
					{
						return res.status(200).json(band);	
					}			
			}
		catch(error)
			{
				return res.status(400).send(error);
			}
				
	}

async function createBand(req,res)
	{
		const band = getModel('Band');
		const data = new band(req.body);
		await data.save();
		return res.status(200).send("Band created sucessfully! :)");
	}

async function updateBand(req,res)
	{
		try
			{
				const band = await getModel('Band').findOne({where:{nombre:req.body.nombre}});
				await band.update({nombre:req.body.nombre, integrantes:req.body.integrantes, fecha_inicio:req.body.fecha_inicio, fecha_separacion:req.body.fecha_separacion, pais:req.body.pais});
				return res.status(200).send("The band was updated correctly. :)");	
			}
		catch(error)
			{
				return res.status(400).send(error);
			}
	}

async function deleteBand(req,res)
	{
		try
			{
				const band = await getModel('Band').findOne({where:{nombre:req.body.nombre}});
				await band.destroy();
				return res.status(200).send("The band was deleted correctly. :)");	
			}
		catch(error)
			{
				return res.status(400).send(error);
			}
	}

module.exports = {createBand, getBands, getBandsNombre, deleteBand, updateBand};