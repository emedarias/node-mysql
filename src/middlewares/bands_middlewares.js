const {getModel} = require('../database/index.js');

async function bandRegistered(req,res,next)
	{
		try
			{
				const band = await getModel('Band').findAll({attributes:['nombre'], where:{nombre:req.body.nombre}});
				if(band.length>0)
					{
						return res.status(404).send('The band is already registered.');
					}
				else
					{
						return next();
					}
						
			}
		catch(error)
			{
				return res.send(error);
			}
	}

async function bandExist(req,res,next)
	{
		try
			{
				const band = await getModel('Band').findOne({attributes:['nombre'], where:{nombre:req.body.nombre}});
				if(band===null)
					{
						return res.status(404).send('The band is not registered.');
					}
				else
					{
						return next();
					}
						
			}
		catch(error)
			{
				return res.send(error);
			}
	}

module.exports={bandRegistered, bandExist};