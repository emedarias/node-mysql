const express = require('express');
const {config} = require('dotenv');
const {connect} = require('./database/index.js');
const {routerBands} = require('./routers/router_bands.js');

async function main() {
	config();
	const server = express();
	//permite enviar la informacion por json.
	server.use(express.json());
	//permite enviar la informacion por fomularios.
	server.use(express.urlencoded({extended:false}));

	const PORT = process.env.PORT || 3000;
	const {DB_PORT, DB_NAME, DB_USERNAME, DB_HOST, DB_PASSWORD,} = process.env;
	
	const isDdOk = connect(DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DB_NAME);
	if(isDdOk)
		{
			const version ='/music/v1';
			server.use(version,routerBands());
			server.listen(PORT,()=>{console.log("Server is running.")});		
		}
	else
		{
			console.log("Db is failing.")
		}
}

main();