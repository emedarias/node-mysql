const express = require('express');
const {createBand, getBands, getBandsNombre, deleteBand, updateBand} = require('../functions/bands_functions.js');
const {bandRegistered, bandExist} = require('../middlewares/bands_middlewares.js');

function routerBands()
	{
		const router = express.Router();
		router.get('/bands/', getBands);
		router.get('/bands/:nombre', getBandsNombre);
		router.post('/bands/create', bandRegistered,createBand);
		router.delete('/bands/delete', bandExist,deleteBand);
		router.put('/bands/update', bandExist,updateBand);
		return router;
	}

module.exports={routerBands};